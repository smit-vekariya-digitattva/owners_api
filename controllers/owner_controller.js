const fs = require("fs");
const path = require("path");
const Owner = require("../models/owner");

module.exports.getOwners = async (req, res, next) => {
  try {
    // Pagination
    let query = Owner.find();

    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;
    const startIndex = (page - 1) * limit; // 2 - 1 * 1 = 1
    const endIndex = page * limit; // 2 * 1 = 2
    const total = await Owner.countDocuments();

    query = query.skip(startIndex).limit(limit);
    const pagination = {};
    if (endIndex < total) {
      // 2 < 4 -> true
      pagination.next = {
        page: page + 1, // 3
        limit, // 1
      };
    }
    if (startIndex > 0) {
      // 1 > 0 -> true
      pagination.previous = {
        page: page - 1, // 1
        limit, // 1
      };
    }
    const owners = await query;
    if (!owners) {
      return res.status(404).json({
        success: true,
        err: {
          message: "No owners found",
        },
      });
    }
    return res.status(200).json({
      success: true,
      totalResults: owners.length,
      pagination,
      data: {
        owners,
      },
    });
  } catch (err) {
    return res.status(500).json({
      success: false,
      error: {
        message: "Internal server error",
      },
    });
  }
};

module.exports.createOwner = async (req, res, next) => {
  try {
    let photo;
    if (req.files) {
      photo = req.files.photo;

      if (!photo.mimetype.startsWith("image/")) {
        return res.status(400).json({
          success: false,
          err: {
            message: "Please upload an image file...",
          },
        });
      }

      if (photo.size > 100000) {
        return res.status(400).json({
          success: false,
          err: {
            message: "Please upload an image less than 1mb...",
          },
        });
      }
      photo.name = `${Date.now()}_${photo.name}`;
      req.body.photo = photo.name;
    }
    const owner = await Owner.create(req.body);
    if (photo) {
      photo.mv(`${process.env.PHOTO_UPLOAD_PATH}/${photo.name}`, (err) => {
        if (err) {
          return res.status(500).json({
            success: false,
            err: {
              message: "problem with file upload",
            },
          });
        }
      });
    }

    return res.status(201).json({
      success: true,
      owner,
    });
  } catch (err) {
    let message, statusCode;

    if (err.name === "MongoError" && err.code === 11000) {
      console.log(err);
      message = `${err.keyValue.email} is already exist`;
      statusCode = 400;
    } else if (err.name === "ValidationError") {
      message = Object.values(err.errors)
        .map((val) => val.message)
        .toString();
      statusCode = 400;
    } else {
      console.log(err);
      message = "Internal server error";
      statusCode = 500;
    }
    return res.status(statusCode).json({
      success: false,
      err: {
        message,
      },
    });
  }
};

module.exports.getOwner = async (req, res, next) => {
  try {
    const owner = await Owner.findById(req.params.id);
    if (!owner) {
      return res.status(404).json({
        success: true,
        err: {
          message: `No owner found with id ${req.params.id}`,
        },
      });
    }
    return res.status(200).json({
      success: false,
      data: {
        owner,
      },
    });
  } catch (err) {
    return res.status(400).json({
      success: false,
      err: {
        message: `Invalid owner id ${req.params.id}`,
      },
    });
  }
};

module.exports.updateOwner = async (req, res, next) => {
  try {
    let owner = await Owner.findById(req.params.id);
    if (!owner) {
      return res.status(404).json({
        success: true,
        err: {
          message: `No owner found with id ${req.params.id}`,
        },
      });
    }
    owner = await Owner.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    return res.status(200).json({
      success: true,
      data: {
        owner,
      },
    });
  } catch (err) {
    let message, statusCode;
    if (err.name === "MongoError" && err.code === 11000) {
      message = `${err.keyValue.email} is already exist`;
      statusCode = 400;
    } else if (err.name === "ValidationError") {
      message = Object.values(err.errors)
        .map((val) => val.message)
        .toString();
      statusCode = 400;
    } else {
      message = `Invalid owner id ${req.params.id}`;
      statusCode = 400;
    }
    return res.status(statusCode).json({
      success: false,
      err: {
        message,
      },
    });
  }
};

module.exports.deleteOwner = async (req, res, next) => {
  try {
    const owner = await Owner.findById(req.params.id);
    if (!owner) {
      return res.status(404).json({
        success: true,
        err: {
          message: `No owner found with id ${req.params.id}`,
        },
      });
    }
    await Owner.findByIdAndDelete(req.params.id);
    if (owner.photo !== "user.png") {
      fs.unlink(
        `${path.join(__dirname, "..", "public", "images", owner.photo)}`,
        (err) => {
          if (err) console.log(err);
          console.log("file deleted...");
        }
      );
    }

    return res.status(204).json({
      success: true,
      data: {},
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      success: false,
      err: {
        message: `Invalid owner id ${req.params.id}`,
      },
    });
  }
};

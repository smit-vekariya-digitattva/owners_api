const express = require("express");
const router = express.Router();
const {
  getOwners,
  createOwner,
  getOwner,
  updateOwner,
  deleteOwner,
} = require("../controllers/owner_controller");

/**
 * @swagger
 * definitions:
 *  Owner:
 *    type: object
 *    required:
 *      - name
 *      - email
 *      - mobile_number
 *      - hobbies
 *      - gender
 *    properties:
 *      name:
 *        type: string
 *        description: Owner name
 *        example: 'smit vekariya'
 *      email:
 *        type: string
 *        format: email
 *        description: Email of the Owner
 *        example: 'smit.vekariya@xyz.com'
 *      mobile_number:
 *        type: string,
 *        description: mobile_number of the Owner
 *        example: '1234567890'
 *      gender:
 *        type: string
 *        description: Gender of the owner
 *        example: 'male'
 *        enum: [male, female, other]
 *      bio:
 *        type: string
 *        description: Biography of the owner
 *        example: 'hello, i am a full stack web developer'
 *      hobbies:
 *        type: array
 *        items:
 *          type: string
 *        description: hobbies of the owner
 *        example: ['reading', 'writing']
 *        enum: [reading, writing, designing, swimming, developing, skydiving]
 *      role:
 *        type: string
 *        description: role of the owner
 *        enum: [student, teacher, contractor]
 *        example: 'student'
 *      created_at:
 *        type: date
 *        description: Date when owner is created
 *        example: '2020-10-02'
 *
 */

router
  .route("/")

  /**
   * @swagger
   * /api/v1/owners:
   *   get:
   *     tags:
   *        - owners
   *     description: Returns all owners
   *     produces:
   *        - application/json
   *     parameters:
   *        - in: query
   *          name: page
   *          description: page number
   *        - in: query
   *          name: limit
   *          description: limits data per page
   *     responses:
   *       200:
   *         description: An array of owners
   *       404:
   *         description: No owners found
   *
   */
  .get(getOwners)
  /**
   * @swagger
   * /api/v1/owners:
   *   post:
   *    tags:
   *      - owners
   *    description: Create new owner
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: owner
   *        description: owner object
   *        in: body
   *        required: true
   *        type: object
   *        schema:
   *          $ref: '#definitions/Owner'
   *    responses:
   *      201:
   *        description: Successfully created with owner info
   *      400:
   *        description: If invalid data provided
   *
   */
  .post(createOwner);
router
  .route("/:id")
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  get:
   *    tags:
   *      - owners
   *    decription: Reaturn single owner
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: id
   *        description: Owner Id
   *        in: path
   *        required: true
   *        type: string
   *    responses:
   *      200:
   *        description: A single owner
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   */
  .get(getOwner)
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  put:
   *    tags:
   *      - owners
   *    description: Update owner info
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: id
   *        description: Owner Id
   *        in: path
   *        required: true
   *        type: string
   *      - name: owner
   *        description: owner object
   *        in: body
   *        required: true
   *        type: object
   *        schema:
   *          $ref: '#definitions/Owner'
   *    responses:
   *      200:
   *        description: Owners updated info
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   *
   */
  .put(updateOwner)
  /**
   * @swagger
   * /api/v1/owners/{id}:
   *  delete:
   *    tags:
   *      - owners
   *    description: delete owner
   *    parameters:
   *      - name: id
   *        description: owner Id
   *        in: path
   *        required: true
   *        type: string
   *    responses:
   *      204:
   *        description: Owner deleted
   *      404:
   *        description: No owner found
   *      400:
   *        description: If passing Invalid id
   */
  .delete(deleteOwner);

module.exports = router;

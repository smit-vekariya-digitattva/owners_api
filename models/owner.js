const mongoose = require("mongoose");
const validator = require("validator");

const ownerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "name must be required"],
    trim: true,
  },
  email: {
    type: String,
    required: [true, "email must be required"],
    unique: true,
    trim: true,
    validate: [validator.isEmail, "please provide a valid email"],
  },
  mobile_number: {
    type: String,
    maxlength: [10, "please provide valid mobile number"],
    minlength: [10, "please provide valid mobile number"],
    required: [true, "mobile number must be required"],
  },
  bio: {
    type: String,
    maxlength: [500, "maximum 500 char. allowd"],
  },
  active_status: {
    type: Boolean,
    default: false,
  },
  hobbies: {
    type: [String],
    required: [true, "hobbies must be required"],
    enum: {
      values: [
        "reading",
        "writing",
        "designing",
        "swimming",
        "developing",
        "skydiving",
      ],
      message:
        "hobbies values either reading, writing, designing, swimming, developing, skydiving",
    },
  },
  role: {
    type: String,
    default: "student",
    enum: {
      values: ["teacher", "student", "contractor"],
      message: "role is either : teacher, student, contractor",
    },
  },
  gender: {
    type: String,
    required: [true, "gender must be required"],
    enum: {
      values: ["male", "female", "other"],
      message: "gender is either : male, female, other",
    },
  },
  photo: {
    type: String,
    default: "user.png",
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Owner", ownerSchema);

require("dotenv").config();
const path = require("path");

const express = require("express");
const fileUpload = require("express-fileUpload");
const mongoose = require("mongoose");
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const app = express();

mongoose
  .connect(process.env.MONGODB, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((con) => console.log("Database Connected"))
  .catch((err) => console.log("Database Connection failed"));

// Routes
const ownerRoute = require("./routes/owner");

// Swagger
const options = {
  swaggerDefinition: {
    info: {
      title: "Owners API",
      version: "1.0.0",
      description: "This api is used to make CRUD operations on owners...",
    },
    host: "127.0.0.1:3000",
    basepath: "/",
  },
  apis: ["./routes/*.js"],
};

const swaggerSpecification = swaggerJsdoc(options);
// console.log(swaggerSpecification);
// Middelwares
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(fileUpload());
app.use("/api/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpecification));
app.get("/", (req, res, next) => {
  res.redirect("/api/docs");
});
app.use("/api/v1/owners", ownerRoute);

app.listen(
  process.env.PORT,
  console.log(`Server is running on port ${process.env.PORT}...`)
);
